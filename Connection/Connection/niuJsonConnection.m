//
//  niuJsonConnection.m
//  YouTube
//
//  Created by Pau Borrell on 10/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "niuJsonConnection.h"

@implementation niuJsonConnection

@synthesize receivedData,connection;

- (id)delegate {
    return delegate;
}

- (void)setDelegate:(id)newDelegate {
    delegate = newDelegate;
}

- (void)initWithURL:(NSURL*)url{
    receivedData = [NSMutableData data];
    NSURLRequest *request=[NSURLRequest requestWithURL:url];
    connection = [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void)initWithSURl:(NSURL*)surl{}

- (void)connectionDidFinish:(NSDictionary *)json 
{
}

- (void)connectionDidFail: (NSError*)error
{
}

- (void)connection:(NSURLConnection *)theConnection didReceiveResponse:(NSURLResponse *)response
// A delegate method called by the NSURLConnection when the request/response 
// exchange is complete.  We look at the response to check that the HTTP 
// status code is 2xx and that the Content-Type is acceptable.  If these checks 
// fail, we give up on the transfer.
{
#pragma unused(theConnection)
    NSHTTPURLResponse * httpResponse;
    NSString *          contentTypeHeader;
    
    assert(theConnection == connection);
    
    httpResponse = (NSHTTPURLResponse *) response;
    assert( [httpResponse isKindOfClass:[NSHTTPURLResponse class]] );
    
    if ((httpResponse.statusCode / 100) != 2) 
    {
        //ERROR html
    } 
    else 
    {
        contentTypeHeader = [httpResponse.allHeaderFields objectForKey:@"Content-Type"];
        if (contentTypeHeader == nil) 
        {
            //No Content-Type
        } 
        else 
        {
            /*if ( ! [contentTypeHeader isHTTPContentType:@"image/jpeg"] 
             && ! [contentTypeHeader isHTTPContentType:@"image/png"] 
             && ! [contentTypeHeader isHTTPContentType:@"image/gif"] ) {
             }*/
            //TOT OK!!!
        } 
    }
}    
- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)data
// A delegate method called by the NSURLConnection as data arrives.  We just 
// write the data to the file.
{
#pragma unused(theConnection)
    assert(theConnection == connection);
    
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)theConnection didFailWithError:(NSError *)error
// A delegate method called by the NSURLConnection if the connection fails. 
// We shut down the connection and display the failure.  Production quality code 
// would either display or log the actual error.
{
#pragma unused(theConnection)
#pragma unused(error)
    //Connection failed
    assert(theConnection == connection);
    
    //Call error delegate
    if ( [delegate respondsToSelector:@selector(connectionDidFail:)] ) {
        [delegate connectionDidFail:error];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)theConnection
// A delegate method called by the NSURLConnection when the connection has been 
// done successfully.  We shut down the connection with a nil status, which 
// causes the image to be displayed.
{
#pragma unused(theConnection)
    assert(theConnection == connection);
    
    //parse out the json data
    NSError* error;
    NSDictionary* json = [NSJSONSerialization 
                          JSONObjectWithData:receivedData
                          
                          options:kNilOptions 
                          error:&error];
    
    if(error)
    {
        NSLog(@"Error: %@",error);
    }
    //Call didFinish delegate
    if ( [delegate respondsToSelector:@selector(connectionDidFinish:)] ) {
        [delegate connectionDidFinish:json];
    }
}


@end
