//
//  niuJsonConnection.h
//  YouTube
//
//  Created by Pau Borrell on 10/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface niuJsonConnection : NSObject
{
    id delegate;
    NSMutableData *receivedData;
    NSURLConnection *connection;
}
- (id)delegate;
- (void)setDelegate:(id)newDelegate;
- (void)initWithURL:(NSURL*)url;
- (void)initWithSURl:(NSURL*)surl;
- (void)connectionDidFinish:(NSDictionary*)json;
- (void)connectionDidFail: (NSError*)error;

@property (nonatomic,retain) NSMutableData *receivedData;
@property (nonatomic,retain) NSURLConnection *connection;

@end
